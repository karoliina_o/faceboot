require('angular');
require('angular-ui-router');
require('angular-resource');
require('font-awesome/css/font-awesome.css');
require('lodash');

require('./assets/styles/index.css');

// create the app module
angular.module('app', ['ngResource', 'ui.router'])
    .config(function ($stateProvider, $locationProvider, $urlRouterProvider) {
        // get rid of # in the URL
        $locationProvider.html5Mode(true);

        // redirect unknown URL's to /
        $urlRouterProvider.otherwise('/');
    });

// require all *.component.js and *.service.js files
const context = require.context('.', true, /\.(component|service)\.js$/);
context.keys().forEach(context);