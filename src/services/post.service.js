angular.module('app')
    .service('posts', function($resource, users) {
        var self = this;

        var Posts = $resource('/api/posts');

        this.posts = null; // cache posts

        this.getAllPosts = function () {
            if (!self.posts) { // cache is empty
                return Posts.query((res) => {
                    self.posts = res;
                }).$promise;
            }
            else { // posts were already cached
                return new Promise((resolve, reject) => {
                    resolve(self.posts);
                });
            }
        };

        this.addComment = function (post, commentText) {
            // find out who is making the comment
            users.getCurrentUser().then((user) => {
                // update the posts cache
                post.comments.push({
                    commentText: commentText,
                    date: (new Date()).toISOString(),
                    userId: user.id,
                    username: user.username
                });
            })

            // to add persistence, could add a REST endpoint for saving the comment and POST to it here
        }
    });