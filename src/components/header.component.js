angular.module('app')
    .component('myHeader', {
        template: `<header class="page-header">
        <div class="navbar">
            <a class="logo" href="http://www.facebook.com"></a>
            <ul class="nav-h">
                <li ng-if="$ctrl.currentUser">
                    <a class="btn profile profile--hover-bg"
                        ng-href="{{ 'profile/' + $ctrl.currentUser.id }}">
                        <img src="assets/profile.jpg">
                        <span>{{ $ctrl.currentUser.username }}</span>
                    </a>
                </li>
                <li>
                    <a class="btn" href="http://www.google.ca">
                        <i class="fa fa-sign-out" aria-hidden="true"></i>
                        <span>Log Out</span>
                    </a>
                </li>
            </ul>
        </div>
    </header>`,
    controller: function (users) {
        var self = this;

        this.$onInit = function () {
            // retrieve the current user
            users.getCurrentUser().then(function (user) {
                self.currentUser = user;
            });
        }
    }
    });