angular.module('app')
    .config(function($stateProvider) {
        $stateProvider.state('profile', {
            url: '/profile/:id',
            component: 'myProfile',
            resolve: {
                user: function (users, $transition$) {
                    return users.getUser($transition$.params().id);
                }
            }
        })
    })
    .component('myProfile', {
        bindings: {
            user: '<'
        },
        template: `<my-header></my-header>
            <div class="profile-container">
            <h2>{{ $ctrl.user.username }}</h2>
            <img ng-src="{{ 'assets/' + $ctrl.user.profilePicture }}">
            <div>
                Hometown: {{ $ctrl.user.hometown }}
            </div>
            <div>
                <a ng-href="/">Back to main page</a>
            </div>
        </div>`,
        controller: function () {}
    });