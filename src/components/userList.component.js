angular.module('app')
    .component('myUserList', {
        template: `<div class="aside">
            <h2>Users</h2>
            <div ng-repeat="user in $ctrl.users" class="user-list-item">
                <a ng-href="{{ 'profile/' + user.id }}">{{ user.username }}</a><br>
            </div>
        </div>`,
        controller: function (users) {
            var self = this;

            // retrieve users from service
            users.getAllUsers().then(function (response) {
                self.users = response;
            });
        }
    });