const path = require('path');
const CopyWebpackPlugin = require("copy-webpack-plugin");
const root = path.resolve(__dirname, 'src');

module.exports = {
	devtool: 'inline-source-map',
	entry: [
		path.resolve(__dirname, 'src/app.js')
	],
	target: 'web',
	output: {
		path: path.resolve(__dirname, 'src'),
		publicPath: '/',
		filename: 'bundle.js'
	},
	plugins: [
        new CopyWebpackPlugin([
            {
                from: path.join(root, 'assets'),
                to: 'assets',
                ignore: ['*.css'],
            }
		])
	],
	module: {
		rules: [
			{
				test: /\.css$/,
				loaders: ['style-loader','css-loader']
			},
			{
                test: /\.(png|jpg)$/,
                loader: 'url-loader'
			},
			// configure extra loaders for font-awesome icons
			{
				test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
				loader: "url-loader?limit=10000&mimetype=application/font-woff"
			},
			{
				test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
				loader: "url-loader?limit=10000&mimetype=application/font-woff"
			},
			{
				test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
				loader: "url-loader?limit=10000&mimetype=application/octet-stream"
			},
			{
				test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
				loader: "file-loader"
			},
			{
				test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
				loader: "url-loader?limit=10000&mimetype=image/svg+xml"
			}
		]
	}
}