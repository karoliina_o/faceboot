const
	path = require('path'),
	express = require('express'),
	favicon = require('serve-favicon'),
	open = require('open'),
	webpack = require('webpack'),
	_ = require('lodash'),
	webpack_config = require('../../webpack.config.dev')

	app = express(),
	port = 3000,
	compiler = webpack(webpack_config),
	
	userData = require('../../src/assets/users.json'),
	postData = require('../../src/assets/posts.json');

app.use(favicon(path.join(__dirname, '../../src/assets/favicon.png')));
app.use(require('webpack-dev-middleware')(compiler, {
	publicPath: webpack_config.output.publicPath
}));

// define REST endpoints for getting users and posts. posting comments to the server is not supported.
app.get('/api/users', (req, res) => {
	res.json(userData);
});

app.get('/api/users/:userId', (req, res) => {
	const user = _.find(userData, (x) => x.id == req.params.userId);
	res.json(user);
});

app.get('/api/posts', (req, res) => {
	res.json(postData);
});

// serve front end
app.get('/*', (req, res) => res.sendFile(path.join(__dirname, '../../src/index.html')));

app.listen(port, err => err ? console.log(err) : open(`http://localhost:${port}`));