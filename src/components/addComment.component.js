angular.module('app')
    .component('myAddComment', {
        bindings: {
            post: '<'
        },
        template: `<div class="p-input">
            <textarea ng-model="$ctrl.commentText" class="p-input__ta"
                placeholder="Add a comment..."></textarea>
            <a class="btn btn-primary f-right margin-8-v" href="#" ng-click="$ctrl.addComment()">
                POST
            </a>
        </div>`,
        controller: function (posts) {
            var self = this;

            this.commentText = '';

            this.addComment = function () {
                // delegate adding comment to the posts service
                posts.addComment(self.post, self.commentText);
                self.commentText = '';
            }
        }
    });