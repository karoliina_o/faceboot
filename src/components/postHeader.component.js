angular.module('app')
    .component('myPostHeader', {
        bindings: {
            post: '<'
        },
        template: `<div class="post__header flex-between margin-16"></div>
                <a class="profile profile--lg profile--hover-ul"
                    ng-href="{{ 'profile/' + $ctrl.post.userId }}">
                    <img ng-if="$ctrl.user" ng-src="{{ 'assets/' + $ctrl.user.profilePicture }}">
                    <span>{{ $ctrl.post.username }}</span>
                </a>
                <span class="timestamp">{{ $ctrl.post.date | date:"yyyy-MM-dd HH:mm" }}</span>
            </div>`,
        controller: function (users) {
            var self = this;

            this.user = null;

            this.$onInit = function () {
                // retrieve the poster's data from the user service in order to display their profile picture.
                // not a good implementation for performance, but should do for now...

                users.getUser(self.post.userId).then(function (user) {
                    self.user = user;
                });
            }
        }
    });