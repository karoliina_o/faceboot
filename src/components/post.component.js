angular.module('app')
    .component('myPost', {
        bindings: {
            post: '<'
        },
        template: `<my-post-header post="$ctrl.post"></my-post-header>
            <div class="text-md margin-16">
                {{ $ctrl.post.postText }}
            </div>
            <my-post-footer post="$ctrl.post"></my-post-footer>
            <ul class="margin-16 top-divider">
                <div class="comments">
                    <my-comment ng-repeat="c in $ctrl.post.comments" comment="c"></my-comment>
                </div>
                <my-add-comment post="$ctrl.post"></my-add-comment>
            </ul>
        </div>`
    });