angular.module('app')
    .service('users', function($resource) {
        var self = this;

        var Users = $resource('/api/users/:userId', { userId: '@id' });

        // set the current user's ID to be 0
        var currentUserId = 0;

        this.getAllUsers = function () {
            // retrieve user data from REST endpoint using $resource
            return Users.query().$promise;
        };

        // get a specific user by their ID
        this.getUser = function (userId) {
            return Users.get({ userId: userId }).$promise;
        }

        // get the current, "default" user, who has the ID 0
        this.getCurrentUser = function () {
            return self.getUser(currentUserId);
        }
    });