angular.module('app')
    .config(function($stateProvider) {
        $stateProvider.state('root', {
            url: '/',
            component: 'root'
        })
    })
    .component('root', {
        template: `<my-header></my-header>
        <my-container></my-container>
        <my-footer></my-footer>`
    });