angular.module('app')
    .component('myPostFooter', {
        bindings: {
            post: '<'
        },
        template: `<div class="flex-between margin-16">
                <div class="flex">
                    <!-- <a class="btn-icon btn--hover-red" -->
                    <a ng-class="['btn-icon', 'btn--hover-red', { 'btn--active': $ctrl.postLiked }]"
                        href="#" ng-click="$ctrl.toggleLike()">
                        <i class="fa fa-heart fa-lg"></i>
                    </a>
                    <span>{{ $ctrl.post.likes }}</span>
                </div>
                <div class="flex">
                    <a class="btn-icon" href="#"><i class="fa fa-comments-o fa-lg"></i></a>
                    <span>{{ $ctrl.post.comments.length }}</span>
                </div>
            </div>`,
        controller: function () {
            var self = this;

            this.postLiked = false;

            this.toggleLike = function () {
                if (!self.postLiked) {
                    self.post.likes++;
                }
                else {
                    self.post.likes--;
                }

                self.postLiked = !self.postLiked;
            }
        }
    });