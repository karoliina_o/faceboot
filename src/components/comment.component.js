angular.module('app')
    .component('myComment', {
        bindings: {
            comment: '<'
        },
        template: `<li class="flex-col">
                        <div class="flex-between">
                            <a class="profile profile--hover-ul"
                                ng-href="{{ 'profile/' + $ctrl.comment.userId }}">
                                <img ng-if="$ctrl.user" ng-src="{{ 'assets/' + $ctrl.user.profilePicture }}">
                                <span>{{ $ctrl.comment.username }}</span>
                            </a>
                            <span class="timestamp text-sm">
                                {{ $ctrl.comment.date | date:"yyyy-MM-dd HH:mm" }}
                            </span>
                        </div>
                        <p class="text-md margin-8-top">
                            {{ $ctrl.comment.commentText }}
                        </p>
                    </li>`,
        controller: function (users) {
            var self = this;

            this.user = null;

            this.$onInit = function () {
                // retrieve the poster's data from the user service in order to display their profile picture.
                // not a good implementation for performance, but should do for now...

                users.getUser(self.comment.userId).then(function (user) {
                    self.user = user;
                });
            }
        }
    });