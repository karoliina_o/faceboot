angular.module('app')
    .component('myContainer', {
        template: `<div class="container">
            <div class="content">
                <my-post ng-repeat="p in $ctrl.posts" post="p"></my-post>
            </div>
            <my-user-list></my-user-list>
        </div>`,
        controller: function (posts) {
            var self = this;

            // retrieve posts from service
            posts.getAllPosts().then(function (response) {
                self.posts = response;
            });
        }
    });